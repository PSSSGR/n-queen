# Name: Sri Padala
# ID: u424p963


def ask_user_for_input(size_t):
    chess_board = []
    for y in range(size_t):
        row = []
        for x in range(size_t):
            row.append('o')
        chess_board.append(row)
    for y in range(size_t):
        x = int(input("Enter the queen position for the column {}".format(y + 1)))
        chess_board[x][y] = 'x'
    return chess_board


def print_state(state, size_t):
    for y in range(size_t):
        for x in range(size_t):
            print('{}  '.format(state[y][x]), end='')
        print()


def get_conflicts(state, size_t):
    conflicts = []
    for row in range(size_t):
        for col in range(size_t):
            if state[row][col] == 'x' and not check_conflicts((row, col), state, size_t):
                conflicts.append((row, col))
    return conflicts


def check_conflicts(position, state, size_t):
    row, column = position
    possible_directions = [(-1, -1), (-1, 1), (1, 1), (1, -1)]
    for x in range(size_t):
        if x != column and state[row][x] == 'x':
            return False
    for y in range(size_t):
        if y != row and state[y][column] == 'x':
            return False
    for direction in possible_directions:
        row, column = position
        while True:
            row += direction[0]
            column += direction[1]
            if row < 0 or abs(row) >= size_t or column < 0 or abs(column) >= size_t:
                break
            if state[row][column] == 'x':
                return False
    return True
