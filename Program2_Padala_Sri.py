# Name: Sri Padala
# ID: u424p963

import random

from helper import *
from util_functions import *


def min_conflicts(state, size_t) -> (list, bool):
    for i in range(5000):
        num_of_conflict = {}
        predicted_positions = []
        min_conflict = float("inf")
        threshold = 0.2
        conflicts = get_conflicts(state, size_t)
        if len(conflicts) <= 0:
            return state, True
        y, x = random.choice(conflicts)
        for j in range(size_t):
            num_of_conflict[(y, j)] = count_conflicts(state, size_t, (y, j))
        num_of_conflict = {key: value for key, value in sorted(num_of_conflict.items(), key=lambda item: item[1])}
        for key, value in num_of_conflict.items():
            if value <= min_conflict:
                predicted_positions.append(key)
                min_conflict = value
            elif random.random() < threshold:
                predicted_positions.append(key)
        row, col = random.choice(predicted_positions)
        state[y][x] = 'o'
        state[row][col] = 'x'
    return state, False


def main():
    size = int(input("Please enter the size of the chess board(n>=4)"))
    chess_matrix = ask_user_for_input(size)
    print_state(chess_matrix, size)
    chess_matrix, completed = min_conflicts(chess_matrix, size)
    if completed:
        print('\nOne of the solution:')
        print_state(chess_matrix, size)
    else:
        print('\nThe algorithm dod not find the solution')
        print_state(chess_matrix, size)
        print()


if __name__ == "__main__":
    main()
