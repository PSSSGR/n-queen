# Name: Sri Padala
# ID: u424p963


def count_conflicts(state,size_t, position):
    num_of_conflicts = 0
    row, column = position
    possible_directions = [(-1, -1), (-1, 1), (1, 1), (1, -1)]
    for x in range(size_t):
        if x != column and state[row][x] == 'x':
            num_of_conflicts += 1
    for y in range(size_t):
        if y != row and state[y][column] == 'x':
            num_of_conflicts += 1
    for direction in possible_directions:
        row, column = position
        while True:
            row += direction[0]
            column += direction[1]
            if row < 0 or abs(row) >= size_t or column < 0 or abs(column) >= size_t:
                break
            if state[row][column] == 'x':
                num_of_conflicts += 1
    return num_of_conflicts



